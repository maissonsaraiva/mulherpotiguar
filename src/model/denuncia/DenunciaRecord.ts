
export class DenunciaRecord{
    
    //#==============DENUNCIANTE
    autor: string;              
    telefone: string;           

    //#==============VITIMA
    vitima_nome: string;             
    vitima_idade: string;            
    vitima_cor: string;              
    vitima_escolaridade: string;     
    vitima_profissao: string;        
    vitima_caracteristica: string;   
    vitima_endereco: string;         
    vitima_pontoreferencia: string;  

    //#==============AGRESSOR
    tipodenuncia_id: string;      
    enderecoocorrencia: string;   
    bairro: string;               
    municipio_id: string;         
    pontoreferencia: string;      
    caracteristica: string;       
    agressor_escolaridade: string; 
    agressor_profissao: string;    
    agressor_idade: string;        
    descricao: string;  

    atendido: string;             
    datadenuncia: string;         
    
}