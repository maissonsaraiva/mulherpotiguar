import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';

import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Geolocation } from '@ionic-native/geolocation';
import { Network } from '@ionic-native/network';
import { SocialSharing } from '@ionic-native/social-sharing';
import { IonicStorageModule } from '@ionic/storage';
import { SMS } from '@ionic-native/sms';
import { CallNumber } from '@ionic-native/call-number';
import { OpenNativeSettings } from '@ionic-native/open-native-settings';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { WebServiceProvider } from '../providers/web-service/web-service';
import { RedServiceProvider } from '../providers/red-service/red-service';
import { ModulosPage } from '../pages/modulos/modulos';

import { NoticiaListPage } from '../pages/noticia-list/noticia-list';
import { NoticiaFormPage } from '../pages/noticia-form/noticia-form';
import { RedemulherListPage } from '../pages/redemulher-list/redemulher-list';
import { RedemulherFormPage } from '../pages/redemulher-form/redemulher-form';
import { SaudeListPage } from '../pages/saude-list/saude-list';
import { SaudeFormPage } from '../pages/saude-form/saude-form';
import { SaudegrupoListPage  } from '../pages/saudegrupo-list/saudegrupo-list';
import { DenunciaFormPage  } from '../pages/denuncia-form/denuncia-form';

import { SettingsListPage  } from '../pages/settings-list/settings-list';
import { SettingsPanicoFormPage  } from '../pages/settings-panico-form/settings-panico-form';



@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ModulosPage,
    NoticiaListPage,
    NoticiaFormPage,
    RedemulherListPage,
    RedemulherFormPage,
    SaudeListPage,
    SaudeFormPage,
    SaudegrupoListPage,
    DenunciaFormPage,
    SettingsListPage,
    SettingsPanicoFormPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({
      name: '__mydb',
         driverOrder: ['indexeddb', 'sqlite', 'websql']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ModulosPage,
    NoticiaListPage,
    NoticiaFormPage,
    RedemulherListPage,
    RedemulherFormPage,
    SaudeListPage,
    SaudeFormPage,
    SaudegrupoListPage,
    DenunciaFormPage,
    SettingsListPage,
    SettingsPanicoFormPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    InAppBrowser,
    Geolocation,
    Network,
    SocialSharing,
    SMS,
    CallNumber,
    OpenNativeSettings,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    WebServiceProvider,
    RedServiceProvider
  ]
})
export class AppModule {}
