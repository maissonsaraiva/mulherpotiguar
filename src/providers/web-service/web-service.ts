import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the WebServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class WebServiceProvider {

  private feeds: Array<any>;

  constructor(private http: Http) {}

  fetchData(url: string): Promise<any> {
    
    return new Promise(resolve => {

      this.http.get(url).map(res => res.json())
        .subscribe(data => {
          this.feeds = data.dados;          

          resolve(this.feeds);

        }, err => console.log(err));          
    });
  }
}
