import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SettingsPanicoFormPage } from './settings-panico-form';

@NgModule({
  declarations: [
    SettingsPanicoFormPage,
  ],
  imports: [
    IonicPageModule.forChild(SettingsPanicoFormPage),
  ],
})
export class SettingsPanicoFormPageModule {}
