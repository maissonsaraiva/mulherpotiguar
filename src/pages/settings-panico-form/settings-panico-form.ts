import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';

import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { WebServiceProvider } from '../../providers/web-service/web-service';

import { SettingsPanicoRecord } from '../../model/settings-panico/SettingsPanicoRecord';

@IonicPage()
@Component({
  selector: 'page-settings-panico-form',
  templateUrl: 'settings-panico-form.html',
})
export class SettingsPanicoFormPage {

  public dadosWeb: Array<any>;
  private url: string = 'http://ceres.rn.gov.br/v2/app/services/mulher/ValidarCodigoMulherWebservice.class.php?codigo=';  

  public config: SettingsPanicoRecord;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public alertCtrl: AlertController,
    public http: Http, 
    public loadingCtrl: LoadingController,
    public service: WebServiceProvider,
    private storage: Storage) 
  {

    this.config = new SettingsPanicoRecord();
    this.config.codigo_valido = false;

      this.storage.get('config').then(value => {
        
        if(value){

          this.config = value;

        }

      });

      //this.storage.remove('config');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPanicoFormPage');
  }

  saveCongig():void{

    this.storage.set('config',  this.config );

    this.showAlert('Sucesso!', 'configurações salvas');

  }

  validarCodigo ( codigo ):void {
    let loading = this.loadingCtrl.create({
      content: 'Verificando...'
    });

    loading.present();

    this.http.get(this.url + codigo).map(res => res.json())
    .subscribe(data => {
      
            console.log(data);

            if(data['success'] == '1'){
         
                var array = new Array();
                array = data['dados'];

                this.config.codigo_valido = true;
                
                this.config.nome_vitima = array['0'].nome
                this.config.telefone_vitima = array['0'].telefone;
                this.config.mulher_id = array['0'].mulher_id;

                console.log(array['0'].mulher_id);

                loading.dismiss();
                
                this.showAlert('Sucesso!', 'Código valido.' );

            }else{

                loading.dismiss();

                this.showAlert('Erro!', 'Código invalido.' );

            }

    }, err => {

      console.log(err);

      loading.dismiss();

      this.showAlert('Erro!', 'Código invalido.' );

    });    

  }

  showAlert(titulo, msn) {
    let alert = this.alertCtrl.create({
      title: titulo,
      subTitle: msn,
      buttons: ['OK']
    });
    alert.present();
  }

  
  doConfirm() {
    let confirm = this.alertCtrl.create({
      title: 'Salvar configurações?',
      message: '',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Cancelar');
          }
        },
        {
          text: 'Sim',
          handler: () => {
            this.saveCongig();
          }
        }
      ]
    });
    confirm.present()
  }

}
