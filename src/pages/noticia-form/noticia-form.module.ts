import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NoticiaFormPage } from './noticia-form';

@NgModule({
  declarations: [
    NoticiaFormPage,
  ],
  imports: [
    IonicPageModule.forChild(NoticiaFormPage),
  ],
})
export class NoticiaFormPageModule {}
