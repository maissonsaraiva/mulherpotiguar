
import { Component  } from '@angular/core';
import { NavController, LoadingController, IonicPage, NavParams   } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';


@IonicPage()
@Component({
  selector: 'page-noticia-form',
  templateUrl: 'noticia-form.html',
})
export class NoticiaFormPage {

  private feed: any;

  constructor(  
    public navCtrl: NavController, 
    private navParams: NavParams,
    private socialSharing: SocialSharing
  ){      

    this.feed = navParams.get('obj');

  }

  shareNoticia (feed):void {
    
    let message = feed.titulo;
    let subject = feed.titulo;
    let file = '';
    let url = 'http://mulherpotiguar.rn.gov.br/index.php?class=Noticia&key=' + feed.id;
    
    this.socialSharing.share(message, subject, file, url).then(() => {
      // Success!
    }).catch(() => {
      // Error!
    });
     
  }


}