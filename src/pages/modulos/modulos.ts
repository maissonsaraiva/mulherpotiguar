import { Component  } from '@angular/core';
import { NavController, LoadingController, IonicPage, AlertController, ToastController    } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';


import { SMS } from '@ionic-native/sms';
import { CallNumber } from '@ionic-native/call-number';
import { Geolocation } from '@ionic-native/geolocation';
import { Storage } from '@ionic/storage';
import { OpenNativeSettings } from '@ionic-native/open-native-settings';



import { NoticiaListPage } from '../../pages/noticia-list/noticia-list';
import { RedemulherListPage } from '../../pages/redemulher-list/redemulher-list';
import { SaudegrupoListPage  } from '../../pages/saudegrupo-list/saudegrupo-list';
import { DenunciaFormPage  } from '../../pages/denuncia-form/denuncia-form';

import { SettingsListPage  } from '../../pages/settings-list/settings-list';

import { SettingsPanicoRecord } from '../../model/settings-panico/SettingsPanicoRecord';

@IonicPage()
@Component({
  selector: 'page-modulos',
  templateUrl: 'modulos.html',
})
export class ModulosPage {

  public modulos: Array<any>;
  public img: string = 'button_red_01';

  public settings = SettingsListPage;
  
  public config: SettingsPanicoRecord;

  constructor( public navCtrl: NavController, private sms: SMS, private callNumber: CallNumber,
    private geolocation: Geolocation, public http: Http, private storage: Storage, public alertCtrl: AlertController,
    private toastCtrl: ToastController, private openNativeSettings: OpenNativeSettings )
  {      
    
    this.fetchContent();


    //openNativeSettings.open('location');

  }

  ionViewWillEnter(){
    
    this.config = new SettingsPanicoRecord();
    
      this.storage.get('config').then(value => {
      
        if(value){
          this.config = value;
        }

      });

      //this.storage.remove('config');

  }

  fetchContent ():void 
  {

    this.modulos = [
      {
        "nome": "Denúncia", 
        "imagem": "denuncia.png",
        "page": DenunciaFormPage
      }, {
        "nome": "Notícia", 
        "imagem": "noticia.png",
        "page": NoticiaListPage
      }, {
        "nome": "Rede Mulher", 
        "imagem": "redemulher.png",
        "page": RedemulherListPage
      }, {
        "nome": "Saúde", 
        "imagem": "saudegrupo.png",
        "page": SaudegrupoListPage
      }/*, {
        "nome": "Violentômetro", 
        "imagem": "violentometro.png",
        "page": "violentrometoForm"
      }*/
    ];
  
  }

  openPage(page)
  {
    this.navCtrl.push( page );
  }

  panico(){

    if( this.config.codigo_valido && this.config.checkPanico ){
      
      //Animação do butão Panico
      this.eventTradeImg();

      // Chamada da ligação
      setTimeout(() => 
      
        this.callNumber.callNumber("08002812336", true)
        .then(() => console.log('Launched dialer!'))
        .catch(() => console.log('Error launching dialer'))

      , 500);  
      
      //Envio de SMS
      
      if(this.config.checkSms){       
      
        if(this.config.contato1){ 
          setTimeout(() => {        
              this.sms.send( this.config.contato1, 'APP NÉSIA: '+ this.config.nome_contado1 +'! '+ this.config.nome_vitima +' de telefone ' + this.config.telefone_vitima + ' está sendo violentada. Nos ajude com sua assistência.');
          }, 1000);  
        
        }
        
      }
    
      setTimeout(() => {

        this.geolocation.getCurrentPosition().then((resp) => {

          console.log( resp.coords.latitude );
          console.log( resp.coords.longitude );

          //Evio da localização
          this.postWebService(  resp.coords.latitude, resp.coords.longitude );
        
        }).catch((error) => {
          console.log('Error getting location', error);
        });

      }, 1000); 

    }else{
      
      console.log(  !this.config.codigo_valido  );
      console.log(  !this.config.checkPanico  );

      if(  !this.config.codigo_valido )
        this.showAlert('AVISO!','Para utilizar o botão pânico é necessário configurá-lo.', SettingsListPage);        
      else if(!this.config.checkPanico)
        this.showAlert('AVISO!','O botão está desativado.', SettingsListPage);
     
    }

  }

  eventTradeImg(){
    this.img = 'button_red_02';
    setTimeout(() =>  this.img = 'button_red_01', 1000);   
  }

  showAlert(titulo, msn, page) {

      let confirm = this.alertCtrl.create({
        title: titulo,
        message: msn,
        buttons: [
          {
            text: 'ok',
            handler: () => {
             
            }
          },
          {
            text: 'Congigurações',
            handler: () => {
              this.openPage(page);
            }
          }
        ]
      });
      confirm.present();
    }

  postWebService (latitude, longitude ):void {
    
    console.log(this.config);

      var dadosPanico = [{
        "latitude": latitude, 
        "longitude": longitude,
        "mulher_id": this.config.mulher_id
      }];

      var head = new Headers();  
      head.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');  
      let options = new RequestOptions({ headers: head });
      let url = 'http://ceres.rn.gov.br/v2/app/services/mulher/SincPanicoMulherWebservice.class.php';
      
      var  json = 'dados=' + JSON.stringify(dadosPanico);

      this.http.post( url, json,          
        options).map(res => res.json())
          .subscribe(
              data => {
                console.log(data);
              },
              err => {
                console.log("ERROR!: ", err);
              }
          );
  
    }


    presentToast(msn) {
      let toast = this.toastCtrl.create({
        message: msn,
        duration: 3000,
        position: 'top'
      });
    
      toast.onDidDismiss(() => {
        console.log('Dismissed toast');
      });
    
      toast.present();
    }


}
