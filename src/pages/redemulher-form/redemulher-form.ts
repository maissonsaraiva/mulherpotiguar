import { Component, ViewChild, ElementRef  } from '@angular/core';
import { NavController, IonicPage, NavParams   } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';

declare var google;

@IonicPage()
@Component({
  selector: 'page-redemulher-form',
  templateUrl: 'redemulher-form.html',
})
export class RedemulherFormPage {

  private feed: any;

  @ViewChild('map') mapElement: ElementRef;
  map: any;

  constructor(  
    public navCtrl: NavController, 
    private navParams: NavParams,
    public geo: Geolocation
  ){      

    this.feed = navParams.get('obj');

  }

  ionViewDidLoad(){
   
    this.loadMap();
  }
 
  loadMap(){

    let latLng = new google.maps.LatLng( this.feed.latitude , this.feed.longitude);
 
    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
 
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

    this.addMarker();
 
  }


  addMarker(){
    
    console.log('entrou');

     let marker = new google.maps.Marker({
       map: this.map,
       animation: google.maps.Animation.DROP,
       position: this.map.getCenter()
     });
    
     let content = this.feed.nomelocal;          
    
     this.addInfoWindow(marker, content);
    
   }

   addInfoWindow(marker, content){
    
     let infoWindow = new google.maps.InfoWindow({
       content: content
     });
    
     google.maps.event.addListener(marker, 'click', () => {
       infoWindow.open(this.map, marker);
     });
    
   }

}