import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RedemulherFormPage } from './redemulher-form';

@NgModule({
  declarations: [
    RedemulherFormPage,
  ],
  imports: [
    IonicPageModule.forChild(RedemulherFormPage),
  ],
})
export class RedemulherFormPageModule {}
