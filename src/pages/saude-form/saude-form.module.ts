import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SaudeFormPage } from './saude-form';

@NgModule({
  declarations: [
    SaudeFormPage,
  ],
  imports: [
    IonicPageModule.forChild(SaudeFormPage),
  ],
})
export class SaudeFormPageModule {}
