import { Component  } from '@angular/core';
import { NavController, LoadingController, IonicPage, NavParams   } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { WebServiceProvider } from '../../providers/web-service/web-service';
import { Storage } from '@ionic/storage';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';


@IonicPage()
@Component({
  selector: 'page-saude-form',
  templateUrl: 'saude-form.html',
})
export class SaudeFormPage {

  public dadosWeb: Array<any>;
  private url: string = "http://ceres.rn.gov.br/v2/app/services/mulher/SaudeCategoriaItemMulherWebservice.class.php";
  private feed: any;

  shownGroup = null;

  constructor(  
    public navCtrl: NavController, 
    public http: Http, 
    public loadingCtrl: LoadingController,
    public service: WebServiceProvider,
    private navParams: NavParams,
    private storage: Storage
  ){      

    this.feed = navParams.get('obj');

    this.storage.get('saude_' + this.feed.id ).then(value => {
      
      if(value){

        this.dadosWeb = value;
  
      }else{

        this.fetchContent();

      }

    });

  }

  fetchContent ():void {
    let loading = this.loadingCtrl.create({
      content: 'Carregando dados...'
    });

    loading.present();

    this.service.fetchData(this.url + '?saudecategoria_id=' + this.feed.id ).then(data => {    
      
      this.dadosWeb = data;

      this.storage.remove('saude_' + this.feed.id );
      this.storage.set( 'saude_' + this.feed.id ,  data );

      loading.dismiss();

    });

  }

  toggleGroup(group) {
    if (this.isGroupShown(group)) {
        this.shownGroup = null;
    } else {
        this.shownGroup = group;
    }
  };

  isGroupShown(group) {
      return this.shownGroup === group;
  };

}