import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { SettingsPanicoFormPage  } from '../../pages/settings-panico-form/settings-panico-form';


@IonicPage()
@Component({
  selector: 'page-settings-list',
  templateUrl: 'settings-list.html',
})
export class SettingsListPage {

  public list: Array<any>;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.fetchContent();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsListPage');
  }

  openPage(page)
  {
    if(page){
      this.navCtrl.push( page );
    }
    
  }

  fetchContent ():void 
  {
    this.list = [
      {
        "nome": "Botão panico", 
        "page": SettingsPanicoFormPage,
        "icon": "radio-button-off"
      }, {
        "nome": "Termo de uso", 
        "page": "",
        "icon": "document"
      }, {
        "nome": "Sobre", 
        "page": "",
        "icon": "book"
      }, 
    ];
  }

}
