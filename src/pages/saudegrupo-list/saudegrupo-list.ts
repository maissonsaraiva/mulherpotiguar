import { Component  } from '@angular/core';
import { NavController, LoadingController, IonicPage, AlertController    } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { WebServiceProvider } from '../../providers/web-service/web-service';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import { Storage } from '@ionic/storage';

import { SaudeFormPage } from '../../pages/saude-form/saude-form';
  
@IonicPage()
@Component({
  selector: 'page-saudegrupo-list',
  templateUrl: 'saudegrupo-list.html',
})
export class SaudegrupoListPage {

  public dadosWeb: Array<any>;
  private url: string = "http://ceres.rn.gov.br/v2/app/services/mulher/SaudeCategoriaMulherWebservice.class.php";
  private filter: string;

  constructor(  
    public navCtrl: NavController, 
    public http: Http, 
    public loadingCtrl: LoadingController,
    public service: WebServiceProvider,
    private storage: Storage,
    public alertCtrl: AlertController
  ){      

    this.storage.get('saudegrupo').then(value => {
      
      if(value){

        this.dadosWeb = value;
  
      }else{

        this.fetchContent();

      }

    });

  }

  fetchContent ():void {
    let loading = this.loadingCtrl.create({
      content: 'Carregando dados...'
    });

    loading.present();

    this.service.fetchData(this.url).then(data => {    
      this.dadosWeb = data;

      this.storage.remove('saudegrupo');
      this.storage.set('saudegrupo',  data );

      loading.dismiss();
    });

  }

  returnValue (feed):void{
    
    this.navCtrl.push( SaudeFormPage,{ obj: feed }); 

  }

  doRefresh(refresher) {
    
    this.service.fetchData(this.url).then(data => {    
      
      this.dadosWeb = data;

      this.storage.remove('saudegrupo');
      this.storage.set('saudegrupo',  data );

        refresher.complete();
    });

  } 

  showRadio() {
    
    let alert = this.alertCtrl.create();
    alert.setTitle('Categoria');

    alert.addInput({
      type: 'radio',
      label: 'TODOS',
      value: '',
      checked: false
    });

    var json = new Array();
    var i = 1;

    this.dadosWeb.forEach(function(value, index, dados){

        var record = new Object();
        record['value'] = value.nome_saude;

        //Algoritmo para remover a duplicidade no array
        if( index != 0 ){         
          if( json[i - 1 ].value != value.nome_saude ){            
            
            json.push( record );
            i++;

            alert.addInput({
              type: 'radio',
              label: value.nome_saude,
              value: value.nome_saude,
              checked: false
            });

          }
        }else{
          json.push( record );

          alert.addInput({
            type: 'radio',
            label: value.nome_saude,
            value: value.nome_saude,
            checked: false
          });

        } 

    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: data => {
        //this.testRadioOpen = false;
        this.filterCategoria(data);
      }
    });
    alert.present();
  }

  filterCategoria( filter ){

    this.filter = filter;
    
  }

}