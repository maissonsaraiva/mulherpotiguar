import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SaudegrupoListPage } from './saudegrupo-list';

@NgModule({
  declarations: [
    SaudegrupoListPage,
  ],
  imports: [
    IonicPageModule.forChild(SaudegrupoListPage),
  ],
})
export class SaudegrupoListPageModule {}
