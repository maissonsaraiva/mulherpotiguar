import { Component  } from '@angular/core';
import { NavController, LoadingController, IonicPage, Platform , ToastController   } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { WebServiceProvider } from '../../providers/web-service/web-service';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import { NoticiaFormPage } from '../../pages/noticia-form/noticia-form';


import { Subscription} from 'rxjs/Subscription';
  
@IonicPage()
@Component({
  selector: 'page-noticia-list',
  templateUrl: 'noticia-list.html',
})
export class NoticiaListPage {

  private dadosWeb: Array<any>;
  private url: string = "http://ceres.rn.gov.br/v2/app/services/mulher/NoticiaMulherWebservice.class.php";
  private connectSubscription: Network;
  private pla: Platform; 

  private status:any;

  private connect: boolean;

  constructor(  
    private navCtrl: NavController, 
    private http: Http, 
    private loadingCtrl: LoadingController,
    private service: WebServiceProvider,
    private network: Network,
    private platform: Platform,
    private storage: Storage,
    private toast: ToastController
  ){      

    this.storage.get('noticialist').then(value => {
      
      if(value){

        this.dadosWeb = value;
  
      }else{

        this.fetchContent();

      }

    });    

  }

  fetchContent ():void {
    
    let loading = this.loadingCtrl.create({
      content: 'Carregando dados...'
    });

    loading.present();

    this.service.fetchData(this.url).then(data => {    
      
      this.dadosWeb = data;
      
      this.storage.remove('noticialist');
      this.storage.set('noticialist',  data );
      
      loading.dismiss();
    });

  }

  returnValue (feed):void{

    this.navCtrl.push( NoticiaFormPage,{ obj: feed }); 

  }

  doRefresh(refresher) {

    this.service.fetchData(this.url).then(data => {    
      
      this.dadosWeb = data;

      this.storage.remove('noticialist');
      this.storage.set('noticialist',  data );

        refresher.complete();
    });

  }  

  ionViewDidEnter() {
     this.network.onConnect().subscribe(data => {
      console.log(data)
      this.displayNetworkUpdate(data.type);
    }, error => console.error(error));
   
     this.network.onDisconnect().subscribe(data => {
      console.log(data)
      this.displayNetworkUpdate(data.type);
    }, error => console.error(error));
  }

  displayNetworkUpdate(connectionState: string){
    let networkType = this.network.type
    this.toast.create({
      message: `You are now ${connectionState} via ${networkType}`,
      duration: 3000
    }).present();
  }


}