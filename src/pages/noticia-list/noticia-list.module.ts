import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NoticiaListPage } from './noticia-list';

@NgModule({
  declarations: [
    NoticiaListPage,
  ],
  imports: [
    IonicPageModule.forChild(NoticiaListPage),
  ],
})
export class NoticiaListPageModule {}
