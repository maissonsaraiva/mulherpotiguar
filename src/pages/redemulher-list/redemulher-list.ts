import { Component  } from '@angular/core';
import { NavController, LoadingController, IonicPage, AlertController   } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { WebServiceProvider } from '../../providers/web-service/web-service';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import { Storage } from '@ionic/storage';
import { CallNumber } from '@ionic-native/call-number';
import { InAppBrowser } from '@ionic-native/in-app-browser';


import { RedemulherFormPage } from '../../pages/redemulher-form/redemulher-form';
  
@IonicPage()
@Component({
  selector: 'page-redemulher-list',
  templateUrl: 'redemulher-list.html',
})
export class RedemulherListPage {

  public dadosWeb: Array<any>;
  private url: string = 'http://ceres.rn.gov.br/v2/app/services/mulher/RedeMulherWebservice.class.php';
  private filter: string = '';

  constructor(  
    public navCtrl: NavController, 
    public http: Http, 
    public loadingCtrl: LoadingController,
    public service: WebServiceProvider,
    private storage: Storage,
    private iab: InAppBrowser,
    private callNumber: CallNumber,
    public alertCtrl: AlertController
  ){   
    
    
    //setTimeout(() => 

      this.storage.get('redemulher').then(value => {
        
        if(value){

          this.dadosWeb = value;
    
        }else{
          
          this.fetchContent();      

        }

      })

    //, 100);  

  }

  fetchContent ():void {
    let loading = this.loadingCtrl.create({
      content: 'Carregando dados...'
    });

    loading.present();

    this.service.fetchData(this.url).then(data => {    
      this.dadosWeb = data;

      this.storage.remove('redemulher');
      this.storage.set('redemulher', data);

      loading.dismiss();
    });

  }

  returnValue (feed):void{

    this.navCtrl.push( RedemulherFormPage,{ obj: feed }); 

    //https://www.google.com.br/maps/@-5.7365287,-35.2423179,15z

  }

  callPhone( numero ):void{

    this.callNumber.callNumber(numero, false)
    .then(() => console.log('Launched dialer!'))
    .catch(() => console.log('Error launching dialer'))

  }

  doRefresh(refresher) {
    
        this.service.fetchData(this.url).then(data => {    
          
          this.dadosWeb = data;
    
          this.storage.remove('redemulher');
          this.storage.set('redemulher', data);
    
            refresher.complete();
        });
    
  } 


  showRadio() {
    
    let alert = this.alertCtrl.create();
    alert.setTitle('Municipio');

    alert.addInput({
      type: 'radio',
      label: 'TODOS',
      value: '',
      checked: false
    });

    var json = new Array();
    var i = 1;

    this.dadosWeb.forEach(function(value, index, dados){

        var record = new Object();
        record['value'] = value.nome_municipio;

        //Algoritmo para remover a duplicidade no array
        if( index != 0 ){         
          if( json[i - 1 ].value != value.nome_municipio ){            
            
            json.push( record );
            i++;

            alert.addInput({
              type: 'radio',
              label: value.nome_municipio,
              value: value.nome_municipio,
              checked: false
            });

          }
        }else{
          json.push( record );

          alert.addInput({
            type: 'radio',
            label: value.nome_municipio,
            value: value.nome_municipio,
            checked: false
          });

        } 

    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: data => {
        this.filterCategoria(data);
      }
    });
    alert.present();
  }

  filterCategoria( filter ){
    
        this.filter = filter;
        
  }

  itemSelected (feed):void {    
    
    //this.iab.create( 'https://www.google.com.br/maps/place/@' + feed.latitude + ',' + feed.longitude + ',16.79z/ '); 
    
    this.navCtrl.push( RedemulherFormPage,{ obj: feed }); 

    //console.log( feed.latitude );
    //console.log( feed.longitude );
    
  } 
    

}