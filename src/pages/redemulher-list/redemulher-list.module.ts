import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RedemulherListPage } from './redemulher-list';

@NgModule({
  declarations: [
    RedemulherListPage,
  ],
  imports: [
    IonicPageModule.forChild(RedemulherListPage),
  ],
})
export class RedemulherListPageModule {}
