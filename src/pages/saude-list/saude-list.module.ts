import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SaudeListPage } from './saude-list';

@NgModule({
  declarations: [
    SaudeListPage,
  ],
  imports: [
    IonicPageModule.forChild(SaudeListPage),
  ],
})
export class SaudeListPageModule {}
