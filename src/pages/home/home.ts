import { Component, ViewChild  } from '@angular/core';
import { NavController, LoadingController, ActionSheetController, Content  } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { RedServiceProvider } from '../../providers/red-service/red-service';

import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  @ViewChild(Content) content: Content;
 
  public feeds: Array<any>;
  private url: string = "https://www.reddit.com/new.json";
  private newerPosts: string = "https://www.reddit.com/new.json?before=";  
  private olderPosts: string = "https://www.reddit.com/new.json?after=";
 
  public hasFilter: boolean = false;
  public noFilter: Array<any>;

  public searchTerm: string = '';
  public searchTermControl: FormControl;

  constructor(  
    public navCtrl: NavController, 
    public http: Http, 
    public loadingCtrl: LoadingController,
    private iab: InAppBrowser,
    public actionSheetCtrl: ActionSheetController,    
    public service: RedServiceProvider
  ){      
    this.fetchContent();

    this.searchTermControl = new FormControl();
    this.searchTermControl.valueChanges.debounceTime(1000).distinctUntilChanged().subscribe(search => {
      if (search !== '' && search) {
        this.filterItems();
      }
    }) 
    
  }

  fetchContent ():void {
    let loading = this.loadingCtrl.create({
      content: 'Fetching content...'
    });

    loading.present();

    this.service.fetchData(this.url).then(data => {
      this.feeds = data;
      this.noFilter = this.feeds; 
      loading.dismiss();
    });

  }

  doRefresh(refresher) {

    var urlRefresh;    

    try {
         urlRefresh = this.newerPosts + this.feeds[0].data.name;
    }catch(err) {
         urlRefresh = this.url;         
    }

    console.log(urlRefresh);

    this.service.fetchData(urlRefresh).then(data => {
      
      this.feeds = data.concat(this.feeds);        
      this.noFilter = this.feeds;      
      this.hasFilter = false;

      refresher.complete();

    });

  }  

  doInfinite(infiniteScroll) {

    var paramsUrl = (this.feeds.length > 0) ? this.feeds[this.feeds.length - 1].data.name : "";

    this.service.fetchData(this.olderPosts + paramsUrl).then(data => {
      
      this.feeds = this.feeds.concat(data);

      this.noFilter = this.feeds;
      this.hasFilter = false;
      infiniteScroll.complete();

    });

  }   

  itemSelected (feed):void {    
    this.iab.create(feed.data.url);    
  } 
  
  showFilters() :void {

    var actionSheet = this.actionSheetCtrl.create({
      title: 'Filter options:',
      buttons: [
        {
          text: 'Music',
          handler: () => {
            this.feeds = this.noFilter.filter((item) => item.data.subreddit.toLowerCase() === "music");
            this.hasFilter = true;
          }
        },
        {
          text: 'Movies',
          handler: () => {
            this.feeds = this.noFilter.filter((item) => item.data.subreddit.toLowerCase() === "movies");
            this.hasFilter = true;
          }
        },
        {
          text: 'Games',
          handler: () => {
            this.feeds = this.noFilter.filter((item) => item.data.subreddit.toLowerCase() === "gaming");
            this.hasFilter = true;
          }
        },
        {
          text: 'Pictures',
          handler: () => {
            this.feeds = this.noFilter.filter((item) => item.data.subreddit.toLowerCase() === "pics");
            this.hasFilter = true;
          }
        },                
        {
          text: 'Ask Reddit',
          handler: () => {
            this.feeds = this.noFilter.filter((item) => item.data.subreddit.toLowerCase() === "askreddit");
            this.hasFilter = true;
          }
        },        
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            this.feeds = this.noFilter;
            this.hasFilter = false;
          }
        }
      ]
    });

    actionSheet.present();

  }    
  
  
  filterItems() {
    this.hasFilter = false;
    this.feeds = this.noFilter.filter((item) => {
        return item.data.title.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1;
    });
  }


}
