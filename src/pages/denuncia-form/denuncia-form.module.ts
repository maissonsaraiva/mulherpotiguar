import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DenunciaFormPage } from './denuncia-form';

@NgModule({
  declarations: [
    DenunciaFormPage,
  ],
  imports: [
    IonicPageModule.forChild(DenunciaFormPage),
  ],
})
export class DenunciaFormPageModule {}
