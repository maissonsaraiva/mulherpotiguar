import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController  } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

import { NoticiaFormPage } from '../../pages/noticia-form/noticia-form';

import { DenunciaRecord } from '../../model/denuncia/DenunciaRecord';

@IonicPage()
@Component({
  selector: 'page-denuncia-form',
  templateUrl: 'denuncia-form.html',
})
export class DenunciaFormPage {

  public record: DenunciaRecord;

  constructor(public navCtrl: NavController, public alerCtrl: AlertController, public http: Http) {

    this.record = new DenunciaRecord();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DenunciaFormPage');
  }


  doConfirm() {
    let confirm = this.alerCtrl.create({
      title: 'Confirmar DENÚNCIA?',
      message: '',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Cancelar');
          }
        },
        {
          text: 'Sim',
          handler: () => {
            this.postWebService();
          }
        }
      ]
    });
    confirm.present()
  }


  postRecord ():void {
  
    if( this.isValidation ){

      this.doConfirm();

    }else{
      
      let alert = this.alerCtrl.create({
        title: 'AVISO!',
        subTitle: 'Todos os campos em vermelho devem ser preenchidos.',
        buttons: ['OK']
      });
      alert.present();

    }

  }



  postWebService ():void {
    
      var head = new Headers();
  
      head.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
  
      let options = new RequestOptions({ headers: head });
      
      var  json = 'dados=' + JSON.stringify(this.record);
      this.http.post('http://devceres.govrn/ceres/app/services/mulher/SincDenunciaMulherWebservice.class.php', 
                            
                            json,
                            options).map(res => res.json())
                             .subscribe(
                                 data => {
                                   console.log(data);
                                 },
                                 err => {
                                   console.log("ERROR!: ", err);
                                 }
                             );
  
    }

  isValidation ():Boolean {

      var validation:boolean = true;

      //document.getElementById('').value

      return validation;

  }

}
