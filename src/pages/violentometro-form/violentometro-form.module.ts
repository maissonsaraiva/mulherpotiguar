import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViolentometroFormPage } from './violentometro-form';

@NgModule({
  declarations: [
    ViolentometroFormPage,
  ],
  imports: [
    IonicPageModule.forChild(ViolentometroFormPage),
  ],
})
export class ViolentometroFormPageModule {}
